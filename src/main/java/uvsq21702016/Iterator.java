package uvsq21702016;

public interface Iterator {
	 public boolean hasNext();
	   public Object next();
}
